@extends('layouts.app')

@section('content')
<h1>Contact</h1>
{!! Form::open(array('url' => 'contact/submit')) !!}
    <div class="form-group">
        {{Form::label('name', 'Name')}}
        {{Form::text('name', null,array('placeholder' => 'Enter name' , 'class' => 'form-control'))}}
    </div>    
    <div class="form-group">
        {{Form::label('email', 'E-Mail Address')}}
        {{Form::text('email', null , array('placeholder' => 'example@gmail.com','class' => 'form-control'))}}
    </div>
    <div class="form-group">
            {{Form::label('message', 'Message')}}
            {{Form::textarea('message', null , array('placeholder' => 'Enter message','class' => 'form-control') )}}
    </div>
    <div>
        {{Form::submit('submit',array('class' => 'btn btn-primary'))}}
    </div>
{!! Form::close() !!}    
@endsection
